/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studentinfogsonapp;

/**
 *
 * @author andyr
 */ 

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class StudentInfoGsonApp {
    
    public static void main(String[] args) {
        
         String[] studentInfo = {"John, Doe, 3.1, 800-555-1212, none",
              "Jane, Deere, 3.25, 808-555-1212, sleeping;dreaming",
              "Sam, Spade, 2.9, 888-555-1212, coffee-drinking;Java;Python"};
    
         //the StudentList constructor takes in the hard coded String array
         StudentList studentList = new StudentList(studentInfo);
         //Then takes that passed in information and populates the list with Student objects
         studentList.AddStudentsToList();
        
         
         for (Student student : studentList.studentList)
            System.out.println(student.InfoToString());
         System.out.println("Done");
         
         //Code from Vogella
         List<Task> list = new ArrayList<Task>();
                for (int i = 0; i < 20; i++) {
                        list.add(new Task(i, "Test1", "Test2", Task.Status.ASSIGNED, 10));
                }
                Gson gson = new Gson();
                Type type = new TypeToken<List<Task>>() {}.getType();
                String json = gson.toJson(list, type);
                System.out.println(json);
                List<Task> fromJson = gson.fromJson(json, type);

                for (Task task : fromJson) {
                        System.out.println(task);
                }
                
        List<Student> students = new ArrayList<Student>();
        
    }
    
}
